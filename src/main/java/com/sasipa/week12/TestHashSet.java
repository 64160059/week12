package com.sasipa.week12;

import java.util.HashSet;
import java.util.Iterator;

public class TestHashSet {
    public static void main(String[] args) {
        HashSet<String> Set = new HashSet();
        Set.add("A1");
        Set.add("A2");
        Set.add("A3");
        printSet(Set);

        Set.add("A1");
        printSet(Set);
        System.out.println(Set.contains("A1"));
        Set.remove("A3");
        System.out.println(Set);
        Set.add("A1");

        HashSet<Integer> set2 = new HashSet<>();
        set2.add(1);
        set2.add(2);
        set2.add(3);
        System.out.println(set2);
        set2.add(3);
        System.out.println(set2);
        set2.clear();
        System.out.println(set2);
        
        HashSet<String> set3 = new HashSet<>();
        set3.addAll(Set);
        System.out.println(set3);
    }

    public static void printSet(HashSet<String> set) {
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println();
    }
}
